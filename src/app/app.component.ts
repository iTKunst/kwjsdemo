import { Component } from '@angular/core';
import { Todo } from './todo';
import { TodoDataService } from './todo-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})

export class AppComponent
{
  title = 'app works!';

  constructor(private todoDataService: TodoDataService)
  {
  }

  get todos()
  {
    return this.todoDataService.getAllTodos();
  }

  onAdd(todo: Todo)
  {
    this.todoDataService.addTodo(todo);
  }

  onRemoveTodo(todo: Todo)
  {
    this.todoDataService.deleteTodoById(todo.id);
  }

  onToggleComplete(todo: Todo)
  {
    this.todoDataService.toggleTodoComplete(todo);
  }

}
